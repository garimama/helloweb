package com.mycomp.webapp;

import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.io.StringWriter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class HelloWebAppTest extends Mockito{

    @Test
    public void testServlet() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);       
        HttpServletResponse response = mock(HttpServletResponse.class);    


        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);
        when(response.getWriter()).thenReturn(writer);

        new HelloWebApp().doGet(request, response);
        
        writer.flush(); // it may not have been flushed yet...
        System.out.print(stringWriter.toString());
        assertTrue(stringWriter.toString().contains("Hello World"));
    }

    @Test
    public void testAdd() throws Exception {

        int k= new HelloWebApp().add(5,5);
        assertEquals("Add",k,10);
        
    }
    @Test
    public void testemb() throws Exception {

        int k= new HelloWebApp().rem(8,7);
        assertEquals("Add",k,1);

    }
}
